# DokoniApp Android App

This Android App sends its location to the Dokoni plugin that is running on your computer.
The [Dokoni plugin](https://bitbucket.org/workingDog/dokoniplugin/overview) shows the location
of the Android phone on a 3D map. As your phone is moved, the map is automatically updated.

In technical terms: DokoniApp is a client for the NASA Worldwind,
[Terramenta framework](https://bitbucket.org/teamninjaneer/terramenta/wiki/Home), Dokoni plugin server.
 In other words, the server will listen to the apps for locations messages.

You need to install this DokoniApp onto your (or anyone else) Android phone, and you need to install
and start the Dokoni plugin on your computer. The App will then send its location to
the computer so that friends and family can see where you are.

Ringo Wathelet

## How to install the App

### From Google Play

not yet available

### From your computer

The generic (untested) steps to install DokoniApp on your Android phone:

  - Download the [DokoniApp-xxx.apk](https://bitbucket.org/workingDog/dokoniapp/downloads) file onto your computer.
  - Connect your Android phone to your computer. You may need to install the appropriate drivers for this.
  - From the computer drag the DokoniApp-xxx.apk file to your Android phone.
  - Start the Android phone and install the DokoniApp-xxx.apk file.


## How to use the App

To use this DokoniApp you need to have the [Dokoni plugin](https://bitbucket.org/workingDog/dokoniplugin/overview) installed and the "Monitor" button pressed.
The Dokoni plugin is the server listening to clients on a particular IP and PORT number.
This DokoniApp is a client connecting to the server. Hence the IP and PORT number must match on both systems.

First you should start the Dokoni plugin. This will show what IP address it is on
and you can select the port number, for example 6082.
Use this information to setup the DokoniApp to match those parameters.

Once the IP and PORT number are setup in the DokoniApp, you can test the connection by pressing the
"Test the connection" button. If the Dokoni server is running and the DokoniApp has the correct parameters,
  the results should say "CAN connect".

To start sending your phone locations, press the "Start tracking" button. As your phone location changes,
location messages will be sent to the Dokoni server that will displayed them on the map.

## References

This is an exercise I'm doing to learn programming Android devices. I have used a number of code snippets
from various blogs and forums to create this android app, in particular code from [Ekito](https://github.com/Ekito/MapTracker-Android).

## Libraries

If you are building the DokoniApp from source, you will need the following libraries:

  - [Android-async-http](http://loopj.com/android-async-http/) the Android Asynchronous Http Client.
  - [Android Support Package](http://developer.android.com/tools/extras/support-library.html) to work on every versions of Android SDK

## Technical

The App sends position information as [NMEA 2.3 0183 RMC](http://en.wikipedia.org/wiki/NMEA_0183) sentences.
It also sends the name of the phone owner, the phone number and the device id.
These are packaged into a http GET request, with the following key,value pairs:

  - key=NMEA_0183RMC
  - value=$GPRMC,002456,A,3553.5295,N,13938.6570,E,0.0,43.1,180700,7.1,W,A*3D

  - key=name
  - value=Wathelet

  - key=number
  - value=12345678

  - key=id
  - value=9774d56d682f549c

## Status

Testing in emulation mode (simulated phone) was successful. No real device testing has been done yet.
The user interface is.....obviously designed by an engineer.

I've used Android 4.2.2, java 1.7 using IntelliJ IDEA 12.


