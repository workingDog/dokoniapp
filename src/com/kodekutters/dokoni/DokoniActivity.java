package com.kodekutters.dokoni;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.*;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.*;
import com.loopj.android.http.RequestParams;

import java.io.IOException;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import static android.view.View.OnClickListener;

/**
 * Ringo Wathelet, March 2013
 * <p/>
 * revised: 06-04-2013 to send NMEA 0183 2.3 GPRMC sentence
 */


public class DokoniActivity extends Activity implements LocationListener {

    enum CostStrategy {LOW, HIGH}

    private static final String TAG = "DokoniActivity";
    private static final int NOTIFICATION_ID = 1234;

    private static final int LOC_MIN_TIME = 0;
    private static final int LOC_MIN_DIST = 0;
    private static final CostStrategy DEFAULT_STRATEGY = CostStrategy.LOW;

    private DokoniDeviceUuidFactory mUuidFactory;

    private LocationManager mLocationManager;
    private DokoniHttpClient mClient;
    private DokoniResponseHandler mHandler;
    private CostStrategy mCostStrategy;
    private String mPhoneNumber = "";
    private String mName = "your name";
    private boolean isTracking = false;

    private TextView mPortTV, mIpTV, mLocationTV, mPhoneNumberTV, mTestResultTV, mAddressTV, mNameTV;
    private Button mTestButton;
    private ToggleButton mStartStopButton;
    private ProgressBar mProgressBar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dokonimain);

        mPortTV = (TextView) findViewById(R.id.portNumber);
        mIpTV = (TextView) findViewById(R.id.ipAddress);
        mTestResultTV = (TextView) findViewById(R.id.testResult);
        mAddressTV = (TextView) findViewById(R.id.address);
        mLocationTV = (TextView) findViewById(R.id.location);
        mPhoneNumberTV = (TextView) findViewById(R.id.phoneNumber);
        mNameTV = (TextView) findViewById(R.id.name);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mStartStopButton = (ToggleButton) findViewById(R.id.startStopButton);

        mStartStopButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (mStartStopButton.isChecked()) {
                    startTracking();
//                    Toast.makeText(getBaseContext(), "Tracking started", Toast.LENGTH_SHORT).show();
                } else {
                    stopTracking();
//                    Toast.makeText(getBaseContext(), "Tracking stopped", Toast.LENGTH_SHORT).show();
                }
            }
        });

        mClient = new DokoniHttpClient();
        mHandler = new DokoniResponseHandler(mTestResultTV);
        mCostStrategy = DEFAULT_STRATEGY;

        mProgressBar.setVisibility(View.GONE);

        mUuidFactory = new DokoniDeviceUuidFactory(this);

        mTestButton = (Button) findViewById(R.id.testButton);
        mTestButton.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                testConnection();
            }
        });

        // get the name of the phone owner
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            try {
                final String[] SELF_PROJECTION = new String[]{ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,};
                Cursor cursor = this.getContentResolver().query(ContactsContract.Profile.CONTENT_URI, SELF_PROJECTION, null, null, null);
                if (cursor.moveToFirst()) {
                    // contact id  cursor.getString(0);
                    // name of phone owner ?
                    mName = cursor.getString(1);
                }
            } catch (Exception ex) {
                mName = "no name";
            }
        }

        mNameTV.setText(mName);

        // get the phone number
        TelephonyManager tMgr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
        mPhoneNumber = tMgr.getLine1Number();

        mPhoneNumberTV.setText(mPhoneNumber);
    }

    private void setEnableAll(boolean onof) {
        mTestButton.setEnabled(onof);
        mPortTV.setEnabled(onof);
        mIpTV.setEnabled(onof);
    }

    /**
     * test the connection to determine if a server will receive the request
     */
    protected void testConnection() {
        mTestResultTV.setText("");
        if (!isInternetPresent()) {
            mTestResultTV.setText("CANNOT connect, network not available");
            return;
        }
        try {
            // construct a test request with an NMEA 0183 GPRMC sentence
            RequestParams params = new RequestParams();
            String testSentence = "$GPRMC,002456,A,3553.5295,N,13938.6570,E,0.0,43.1,180700,7.1,W,A*3D";
            params.put("NMEA_0183RMC", testSentence);
            params.put("id", mUuidFactory.getDeviceUuid().toString());
            params.put("name", mNameTV.getText().toString());
            params.put("number", mPhoneNumber);

            // send the test request to the server
            mClient.get(mIpTV.getText().toString().trim(), mPortTV.getText().toString().trim(), params, mHandler);
        } catch (Exception ex) {
            mTestResultTV.setText("CANNOT connect");
        }
    }

    /**
     * test if the internet is present
     *
     * @return true if present else false
     */
    private boolean isInternetPresent() {
        return new ConnectionDetector(getApplicationContext()).isConnectingToInternet();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isTracking) {
            stopTracking();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isTracking) {
            startTracking();
        }
    }

    /**
     * register location listener if it was unregistered
     */

    private void startTracking() {
        Log.d(TAG, "startTracking: request received");
        isTracking = true;
        mProgressBar.setVisibility(View.VISIBLE);
        mTestResultTV.setText("Tracking started");
        mTestButton.setEnabled(false);
        setEnableAll(false);
        if (mLocationManager == null) {
            // get the right location provider following current strategy
            mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            String provider = null;
            switch (mCostStrategy) {
                case LOW:
                    provider = mLocationManager.getBestProvider(createCoarseCriteria(), true);
                    break;
                case HIGH:
                    provider = mLocationManager.getBestProvider(createFineCriteria(), true);
                    break;
            }

            // provider can be null in case we have no location tracking enabled
            if (provider == null) {
                mLocationManager = null;
                mTestResultTV.setText("CANNOT track because no location provider available");
            } else { // otherwise, start tracking
                mLocationManager.requestLocationUpdates(provider, LOC_MIN_TIME, LOC_MIN_DIST, this);
                Log.d(TAG, "startTracking: started");
            }
        }
    }

    /**
     * unregister location listener if it was registered
     */
    private void stopTracking() {
        Log.d(TAG, "stopTracking: request received");
        isTracking = false;
        mProgressBar.setVisibility(View.GONE);
        mTestButton.setEnabled(true);
        setEnableAll(true);
        mTestResultTV.setText("Tracking stopped");
        if (mLocationManager != null) {
            Log.d(TAG, "stopTracking: request processing");
            mLocationManager.removeUpdates(this);
            mLocationManager = null;
            Log.d(TAG, "stopTracking: stopped");
        }
    }

    /**
     * this criteria will settle for less accuracy, high power, and cost
     */
    public static Criteria createCoarseCriteria() {
        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_COARSE);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_HIGH);
        return c;
    }

    /**
     * this criteria needs high accuracy, high power, and cost
     */
    public static Criteria createFineCriteria() {
        Criteria c = new Criteria();
        c.setAccuracy(Criteria.ACCURACY_FINE);
        c.setAltitudeRequired(false);
        c.setBearingRequired(false);
        c.setSpeedRequired(false);
        c.setCostAllowed(true);
        c.setPowerRequirement(Criteria.POWER_HIGH);
        return c;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged: received location update");
        // must have valid lat, lon
        if (isValidLatDeg(location.getLatitude()) && isValidLonDeg(location.getLongitude())) {

            // display notification
            Intent notificationIntent = new Intent(this, DokoniActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this,
                    0, notificationIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);

            NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);

            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.drawable.ic_launcher)
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setTicker("Location updated")
                    .setContentText("Timestamped " + new Time(location.getTime()).toString());
            Notification n = builder.build();

            nm.notify(NOTIFICATION_ID, n);

            // refresh UI
            refreshUI(location);

            // create an NMEA 0183 GPRMC sentence request
            RequestParams params = new RequestParams();
            params.put("NMEA_0183RMC", new NMEA_0183RMC(location).toString());
            params.put("id", mUuidFactory.getDeviceUuid().toString());
            params.put("name", mNameTV.getText().toString());
            params.put("number", mPhoneNumber);

            // send the request to the server
            try {
                mClient.get(mIpTV.getText().toString().trim(), mPortTV.getText().toString().trim(), params, mHandler);
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d(TAG, "onProviderDisabled " + provider);
        stopTracking();
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d(TAG, "onProviderEnabled " + provider);
        startTracking();
    }

    @Override
    public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
        Log.d(TAG, "onStatusChanged");
        stopTracking();
        startTracking();
    }

    private void refreshUI(Location location) {
        mLocationTV.setText(String.valueOf(new DecimalFormat("##.#####").format(location.getLatitude())) + " " +
                String.valueOf(new DecimalFormat("###.#####").format(location.getLongitude())));
        mAddressTV.setText(getAddress(location));
    }

    public String getAddress(Location location) {
        Geocoder geocoder = new Geocoder(this, Locale.ENGLISH);
        String ret = "";
        try {
            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder();
                for (int i = 0; i < returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                ret = strReturnedAddress.toString();
            } else {
                ret = "NO address found";
            }
        } catch (IOException e) {
            ret = "CANNOT get address";
        }
        return ret;
    }

    public boolean isValidLatDeg(double val) {
        if (val >= -90 && val <= 90) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isValidLonDeg(double val) {
        if (val >= -180 && val <= 180) {
            return true;
        } else {
            return false;
        }
    }
}