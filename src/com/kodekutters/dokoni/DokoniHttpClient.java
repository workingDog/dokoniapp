package com.kodekutters.dokoni;

//Static wrapper library around AsyncHttpClient

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class DokoniHttpClient {
	
	private static final String BASE_IP = "1.2.3.4";
    private static final String BASE_PORT = "6082";

	private AsyncHttpClient client;
	
	public DokoniHttpClient() {
		client = new AsyncHttpClient();
		client.addHeader("Content-Type", "application/x-www-form-urlencoded");
	}

	public void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}

    public void get(String url, String port, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get("http://"+url+":"+port, params, responseHandler);
    }

	public void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}

	private static String getAbsoluteUrl(String relativeUrl) {
		return BASE_IP + relativeUrl;
	}
}
