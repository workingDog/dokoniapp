package com.kodekutters.dokoni;

import android.widget.TextView;

import com.loopj.android.http.AsyncHttpResponseHandler;

public class DokoniResponseHandler extends AsyncHttpResponseHandler {

    private TextView mStatusTV;

    public DokoniResponseHandler(TextView statusTV) {
        mStatusTV = statusTV;
    }

    @Override
    public void onSuccess(java.lang.String s) {
        mStatusTV.setText("CAN connect");
        super.onSuccess(s);
    }

    @Override
    public void onStart() {
        mStatusTV.setText("testing...");
        super.onStart();
    }

    @Override
    public void onFailure(Throwable arg0, String arg1) {
        mStatusTV.setText("CANNOT connect to selected IP and PORT number");
        super.onFailure(arg0, arg1);
    }

    @Override
    public void onFinish() {
        super.onFinish();
    }
}
