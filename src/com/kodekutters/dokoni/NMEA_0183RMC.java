package com.kodekutters.dokoni;

import android.location.Location;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * Author: Ringo Wathelet Date: 7/04/13 Version: 1
 */
public class NMEA_0183RMC {

    private StringBuilder sb = new StringBuilder();

    // NMEA recommended minimum specific GPS/Transit data
    // references:
    // http://www.gpsinformation.org/dale/nmea.htm#RMC
    // http://aprs.gids.nl/nmea/
    // http://www.winsystems.com/software/nmea.pdf
    // http://en.wikipedia.org/wiki/NMEA_0183
    //
    // $GPRMC,065411.99,V,0724.339,S,04708.235,E,94.2,17.42,010413,,E*44
    // $GPRMC,092751.000,A,5321.6802,N,00630.3371,W,0.06,31.66,280511,,,A*45
    // $GPRMC,123519,A,4807.038,N,01131.000,E,022.4,084.4,230394,003.1,W*6A
    // $GPRMC,002456,A,3553.5295,N,13938.6570,E,0.0,43.1,180700,7.1,W,A*3D
    //
    //      $            sentence starts with a dollar sign
    //      RMC          Recommended Minimum sentence C, prepended with GP for GPS or GL for GLONASS
    //      123519.12    Fix taken at 12:35:19:12 UTC, optional with milisecs
    //      A            Status A=active or V=Void.
    //      4807.038,N   Latitude 48 deg 07.038' N
    //      01131.000,E  Longitude 11 deg 31.000' E
    //      022.4        Speed over the ground in knots
    //      084.4        Track angle in degrees True
    //      230394       Date - 23rd of March 1994
    //      003.1,W      Magnetic Variation
    //      A            NMEA 2.3 FAA Position System Mode indicator character, A=autonomous, D=differential, E=Estimated, N=not valid, S=Simulator
    //      *6A          The checksum data, always begins with *
    //      \crt\lf      sentence suppose to end with control line feed

    /**
     * a NMEA 2.3 0183 RMC sentence
     *
     * @param location
     */
    public NMEA_0183RMC(final Location location) {

        String latHemis = "N";
        if (location.getLatitude() < 0) {
            latHemis = "S";
        }
        double lat = Math.abs(location.getLatitude());
        int latDeg = (int) lat;
        int latMin = (int) (lat * 60.0) % 60;
        int latSec = (int) (lat * 3600.0) % 60;
        String latString = String.format("%02d%02d.%03d,%s", latDeg, latMin, latSec, latHemis);

        String lonHemis = "E";
        if (location.getLongitude() < 0) {
            lonHemis = "W";
        }
        double lon = Math.abs(location.getLongitude());
        int lonDeg = (int) lon;
        int lonMin = (int) (lon * 60.0) % 60;
        int lonSec = (int) (lon * 3600.0) % 60;
        String lonString = String.format("%03d%02d.%03d,%s", lonDeg, lonMin, lonSec, lonHemis);

        Calendar time = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        time.setTimeInMillis(location.getTime());
        String timeString = String.format("%02d%02d%02d.%02d",
                time.get(Calendar.HOUR_OF_DAY), time.get(Calendar.MINUTE),
                time.get(Calendar.SECOND), time.get(Calendar.MILLISECOND));

        String dateString = String.format("%02d%02d%02d",
                time.get(Calendar.DAY_OF_MONTH), time.get(Calendar.MONTH), time.get(Calendar.YEAR) - 2000);

        // TODO magnetic variation

        String speedKmh = String.format("%.2f", (float) (location.getSpeed() * 3.6)); // from m/s to km/h
//        String speedNm = String.format("%.2f", (float) (location.getSpeed() * 1.943844)); // from m/s to knots
        String course = String.format("%.2f", location.getBearing());

        sb.append("$GPRMC,").append(timeString + ",");
        sb.append("A,").append(latString + ",").append(lonString + ",");
        sb.append(speedKmh + ",").append(course + ",").append(dateString + ",");
        sb.append("000.0,W,").append("A");
        String checkSum = checksum(sb.toString().substring(1, sb.toString().length()));
        sb.append(checkSum);

    }

    public String toString() {
        return sb.toString();
    }

    private String checksum(String nmea) {
        int sum = 0;
        for (int i = 0; i < nmea.length(); i++) {
            sum ^= (byte) nmea.charAt(i);
        }
        return String.format("*%02x\r\n", sum);
    }
}
